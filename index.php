<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=1074" />
<meta name="keywords" content="Rachel Rippy, web, website, design, development, HTML, CSS, wordpress, jquery, javascript, CMS, blog, UI, UX, ecommerce, web app">
<meta name="description" content="Rachel Rippy: web designer and developer based in Denver, CO.">
<link rel="shortcut icon" href="http://www.rachelrippy.com/favicon.ico" type="image/x-icon" /> 

<title>Rachel Rippy - Web Design and Development</title>

<link href="rachelrippysite.css" rel="stylesheet" />

<script src="modernizr-1.7.min.js"></script>

<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19741940-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
<div id="wrapper">
  <div id="header">
	  <div class="header_logo">
          	<a href="http://www.rachelrippy.com/">
            	<img src="images/typewriter.png" alt="Rachel Rippy logo" />
            </a>
          </div>
        <div class="header_info">
          <h1>Rachel Rippy</h1>
		  <h2>Web Design and Development</h2>
          <div class="contact_info"><a href="mailto:rachel@rachelrippy.com">rachel@rachelrippy.com</a><p>303.681.6301</p><p>Denver, CO</p></div>
		  <div class="top_nav">
          	<a href="http://www.rachelrippy.com/" class="main_nav home">WORK</a>
            <a href="about.html" class="main_nav about">ABOUT</a>
            <a href="http://www.rachelrippy.com/blog/" class="main_nav">BLOG</a>
          </div>
      </div>
	</div>
    
    <div class="pop_section_top"></div>
    
    <div id="piece_section">
        <div class="piece_view">
        
        	<div class="piece_details" id="ajax_full_piece"></div>
      
        </div>
    </div>
    
    <div id="main_content">
    	
        <div class="thumb"><a href="postscript.php"><img src="images/postscript_thb.jpg" alt="Postscript website" /></a>
    	  <div class="thumb_box"></div>
                <div class="thumb_desc" id="postscript">
                <h4>Postscript &ndash; Website</h4>
                <p>UI/UX Design, WordPress, HTML5, CSS3, jQuery, PHP</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
          </div>
        </div>
        
    	<div class="thumb"><a href="quimbee.html"><img src="images/quimbee_thb.jpg" alt="Quimbee website" /></a>
       	  <div class="thumb_box"></div>
              <div class="thumb_desc" id="quimbee">
                <h4>Quimbee &ndash; Website</h4>
                <p>UI/UX Design, Video</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
              </div>
        </div>
        
        <div class="thumb"><a href="quri.html"><img src="images/quri_thb.jpg" alt="Quri website" /></a>
       	  <div class="thumb_box"></div>
          	<div class="thumb_desc" id="quri">
            	<h4>Quri &ndash; Website</h4>
          		<p>UI/UX Design, HTML5, CSS3, jQuery</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
            </div>
        </div> 
        
        <div class="thumb"><a href="ctt.html"><img src="images/ctt_thb.jpg" alt="Changethethought website" /></a>
    	  <div class="thumb_box"></div>
                <div class="thumb_desc" id="ctt">
                	<h4>Changethethought &ndash; Website</h4>
                	<p>WordPress, HTML5, CSS3, jQuery, PHP</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
                </div>
        </div>
        
        <div class="thumb"><a href="jared.html"><img src="images/jared_thb.jpg" alt="Jared Rippy website" /></a>
       	  <div class="thumb_box"></div>
          	<div class="thumb_desc" id="jared">
            	<h4>Jared Rippy &ndash; Website</h4>
          		<p>WordPress, HTML5, CSS3, jQuery, PHP</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
            </div>
        </div>
        
        <div class="thumb"><a href="paradox.html"><img src="images/paradox_thb.jpg" alt="Paradox Resources website" /></a>
       	  <div class="thumb_box"></div>
          	<div class="thumb_desc" id="paradox">
            	<h4>Paradox Resources &ndash; Website</h4>
          		<p>UI/UX Design, WordPress, HTML5, CSS3, jQuery, PHP</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
            </div>
        </div>
        
        <div class="thumb"><a href="aam.html"><img src="images/aam_thb.jpg" alt="Aspen Art Museum website" /></a>
    	  <div class="thumb_box"></div>
                <div class="thumb_desc" id="aam">
                	<h4>Aspen Art Museum &ndash; Website</h4>
                	<p>UI/UX Design, HTML, CSS, JavaScript, Flash, Video</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
                </div>
        </div>
        
        <div class="thumb"><a href="new_aam.html"><img src="images/new_building_thb.jpg" alt="New Aspen Art Museum microsite" /></a>
       	  <div class="thumb_box"></div>
          	<div class="thumb_desc" id="new_aam">
            	<h4>New Aspen Art Museum &ndash; Microsite</h4>
          		<p>UI/UX Design, HTML, CSS, JavaScript</p><p>VIEW PROJECT</p><div class="thumb_line"></div>
            </div>
        </div>
        
       
  </div>
  
    <div id="footer">
    	<div class="footer_txt"><p>Specializing in web design and front-end development.</p><p>Copyright &copy; 2013 Rachel Rippy. Images and content may not be used without permission.</p></div>
    </div>
    
</div>

<script src="jquery-1.4.min.js"></script>
<script src="jquery.scrollTo.js"></script>
<script src="script.js"></script>

</body>
</html>
