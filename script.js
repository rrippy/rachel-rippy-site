$(document).ready(function(){
  $('.thumb_box').css({
    // opacity: "0.88",
	cursor: "pointer"
  });
  $('.thumb_desc').css({
	cursor: "pointer"
  });
  
  // mouseover thumbnails
  $('.thumb').hover(function() {
    $(this)
      .find('div.thumb_box')
      .slideToggle(300);
      $(this)
		.find('div.thumb_desc')
		.slideToggle(300);
  // mouseout thumbnails
  }, function () {
	 $(this)
		.find('div.thumb_desc')
		.slideToggle(700);
	$(this)
      .find('div.thumb_box')
      .slideToggle(700);
  });
  
  // prevent default on button click
  $('.thumb a').click(function(e) {
		e.preventDefault();
  });
  
  // on thumbnail click
  $('.thumb_desc').click(function() {
		// slide line down
		var getPiece = $(this).attr('id');
		// use ajax to pull piece info
		var url = (getPiece + '.html') + ' .full_piece_details';
		$('div.piece_view')
			.slideDown('slow');
		// scroll document
		$.scrollTo(0,'slow');
		// fade out existing piece
		$("body").find('.show')
			.fadeOut()
			.removeClass('show');
		// add spinner
		$('<div></div>')
          .attr('class', 'spinner')
          .hide()
          .appendTo('.piece_view')
          .fadeTo('slow', 0.7);
		// fade in piece details
		$('#ajax_full_piece').load(url, function() {
      // set the image hidden by default    
      		$(this).hide();
			$('.spinner').fadeOut().remove();
			$(this).delay(500)
			.fadeIn()
			.addClass('show');
		});
		/*$("body").find('#' + getPiece + '_details')
			.delay(600)
			.fadeIn()
			.addClass('show');*/
  });
  
  // close piece views
  // use .live to refresh document load
  $('.close_btn a').live('click', function(e) {
		// prevent default on button click
		e.preventDefault();
		// fade out existing piece
		$("body").find('.show')
			.fadeOut()
			.removeClass('show');
		// slide line up
		$("body").find('div.piece_view')
			.delay(500)
			.slideUp('slow');
  });
  // use .live to refresh document load
	$('.header_logo a').live('click', function(e) {
		// prevent default on button click
		e.preventDefault();
		// fade out existing piece
		$("body").find('.show')
			.fadeOut()
			.removeClass('show');
		// slide line up
		$("body").find('div.piece_view')
			.delay(500)
			.slideUp('slow');
  });
  // use .live to refresh document load
  $('a.home').live('click', function(e) {
		// prevent default on button click
		e.preventDefault();
		// fade out existing piece
		$("body").find('.show')
			.fadeOut()
			.removeClass('show');
		// slide line up
		$("body").find('div.piece_view')
			.delay(500)
			.slideUp('slow');
  });
  
  // next piece button
  // use .live to refresh document load
   $('a.next').live('click', function(e) {
		e.preventDefault();
		var numImages = $('.show .pieces_inner img').length;
		var currentImage = (Math.abs(parseInt($('.show .pieces_inner').css('left'))) / 686) + 1;
		// scroll left
		if (currentImage < numImages) {
			$('.show .pieces_inner').animate({'left':'-=' + '686'}, 'slow');
			$('.show .current').text(currentImage + 1);
		}
		// scroll to begining
		else {
			$('.show .pieces_inner').animate({'left':'0'}, 'normal');
			$('.show .current').text('1')
		}
  });
  // prev piece button
   // use .live to refresh document load
   $('a.prev').live('click', function(e) {
		e.preventDefault();
		var numImages = $('.show .pieces_inner img').length;
		var currentImage = (Math.abs(parseInt($('.show .pieces_inner').css('left'))) / 686) + 1; 
		// scroll left
		if (currentImage > 1) {
			$('.show .pieces_inner').animate({'left':'+=' + '686'}, 'slow');
			$('.show .current').text(currentImage - 1);
		}
		// scroll to begining
		else {
			var maxPosition = (numImages - 1) * 686;
			$('.show .pieces_inner').animate({'left':'-=' + maxPosition}, 'normal');
			$('.show .current').text(numImages);
		}
  });
   
  // logoSlideshow();
  
  
  // on about click
  $('a.about').click(function(e) {
		e.preventDefault();
		// slide line down
		// use ajax to pull piece info
		var url = ('about.html') + ' .full_piece_details';
		$('div.piece_view')
			.slideDown('slow');
		// scroll document
		$.scrollTo(0,'slow');
		// fade out existing piece
		$("body").find('.show')
			.fadeOut()
			.removeClass('show');
		// add spinner
		$('<div></div>')
          .attr('class', 'spinner')
          .hide()
          .appendTo('.piece_view')
          .fadeTo('slow', 0.7);
		// fade in piece details
		$('#ajax_full_piece').load(url, function() {
      // set the image hidden by default    
      		$(this).hide();
			$('.spinner').fadeOut().remove();
			$(this).delay(500)
			.fadeIn()
			.addClass('show');
		});
		/*$("body").find('#' + getPiece)
			.delay(600)
			.fadeIn()
			.addClass('show');*/
  });
  
  
});


// logo slideshow
/*function logoSlideshow() {
	var current = $('.header_logo .current_logo');
	var next = current.next().length ? current.next() : current.parent().children(':first');
	current.hide().removeClass('current_logo');
	next.fadeIn().addClass('current_logo');
	setTimeout(logoSlideshow, 8000);
}*/