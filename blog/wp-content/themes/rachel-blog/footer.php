		<?php
		    // Use this hook to do things above the footer
		    notesblog_above_footer();

		    // The widgets are in sidebar-footer.php
		    get_sidebar('footer');
		?>
			
		</div><!-- /#blog -->
		
		<div id="copy">
		    <div id="footer_info" class="fullcolumn">
		    	<p>Specializing in web design and front-end development.</p><p>Copyright &copy; 2012 <?php bloginfo('name'); ?>. Images and content may not be used without permission.</p>
		    </div>
		</div>

</div>

<?php
    // Use this hook to do things below the site
    notesblog_below_site();
    
    // WordPress ends
    wp_footer();
?>
</body>
</html>