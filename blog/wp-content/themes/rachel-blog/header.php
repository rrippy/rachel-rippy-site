<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19741940-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<title>
	<?php 
		// Print the right title
		if (is_home () ) { 
			bloginfo('name'); echo ' - Blog';
		} elseif (is_category() || is_tag()) { 
			bloginfo('name'); echo ' - Blog - '; single_cat_title(); 
		} elseif (is_single() || is_page()) { 
			bloginfo('name'); echo ' - Blog - '; single_post_title();
		} else { 
			bloginfo('name'); echo ' - Blog - '; wp_title('',true); 
		}
	?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	// Comment JavaScript
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

	//  Use this hook to insert things into HEAD
	notesblog_inside_head();
	
	// Kick off WordPress
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="site">

	<?php
	    // Use this hook to insert things before the wrap
	    notesblog_above_site();
	?>

	<div id="outer-wrap"><div id="inner-wrap">
        
        <div id="header">
          <div class="header_logo">
          	<a href="http://www.rachelrippy.com/">
            	<img src="http://www.rachelrippy.com/images/typewriter.png" alt="Rachel Rippy logo" />
            </a>
          </div>
            <div class="header_info">
              <h1>Rachel Rippy</h1>
              <h2>Web Design and Development</h2>
              <div class="contact_info"><a href="mailto:rachel@rachelrippy.com">rachel@rachelrippy.com</a><p>303.681.6301</p><p>Denver, CO</p></div>
              <div class="top_nav">
                <a href="http://www.rachelrippy.com/" class="main_nav home">WORK</a>
                <a href="http://www.rachelrippy.com/about.html" class="main_nav about">ABOUT</a>
                <a href="<?php echo home_url(); ?>" class="main_nav">BLOG</a>
              </div>
          </div>
        </div>
        
        <div class="pop_section_top"></div>
        
		<div id="blog">